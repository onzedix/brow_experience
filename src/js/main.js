"use strict";

var EyeDesigner = (function (PIXI) {
    /**
     * Racine DOM
     * @type {Element | null}
     * @private
     */
    var _root = document.querySelector("#thecanvas");

    /**
     * Les valeurs des variables
     * @type {{tint: number, thickness: number, position: number}}
     * @private
     */
    var _values = {
        tint: 0,
        thickness: 0,
        position: 0,
        distance: 0
    };

    /**
     * La variable liée au visage
     */
    var _face;

    /**
     * Y initial des sourcils
     * @type {number}
     * @private
     */
    var _base_y = 0;

    /**
     *
     * @type {number}
     * @private
     */
    var _base_brow_scale = 0.7;

    /**
     * La variable à modifier
     * @type {string}
     * @private
     */
    var _current_variable = "position";

    /**
     * Configuration de la vue PIXI
     * @type {{width: number, height: number, preserveDrawingBuffer: boolean, resolution: number, backgroundColor: number}}
     * @private
     */
    var _config = {
        width: 360,
        height: 640,
        preserveDrawingBuffer: true,
        resolution: 1,
        backgroundColor: 0xffffff,
        forceCanvas: true
    };

    /**
     * L'object PIXI
     */
    var _app;

    /**
     * Le sourcil
     */
    var _brow;

    /**
     * Dessiner l'image du visage tirée du chargement ou de la prise de photo
     * @private
     */
    function _drawFace() {
        _face = new PIXI.Sprite.fromImage(window.localStorage.getItem("image"));
        _app.stage.addChild(_face);
        _face.anchor.set(0.5);
        _face.scale.set(1.2);
        _face.position.set(1 * _root.getBoundingClientRect().width / 2, 1 * _root.getBoundingClientRect().height / 2);
        _removeRealBrow(_face);
    }

    /**
     * Recupère une configuration avec la taille adaptée à l'écran
     * @param c une configuration PIXI
     * @returns {*}
     * @private
     */
    function _calculateStageSize(c) {
        c.width = 1 * _root.getBoundingClientRect().width;
        c.height = 1 * _root.getBoundingClientRect().height;
        return c;
    }

    /**
     * Positionnement initial d'un sourcil
     * @private
     */
    function _initBrowPosition() {
        _brow.anchor = new PIXI.Point(0.5, 0.5);
        _brow.scale.set(0.3);// = new PIXI.Point(_base_brow_scale, _base_brow_scale);
        _base_y = _face.y - 1 / 2 * _face.height + 100;
        _brow.position = new PIXI.Point(_root.getBoundingClientRect().width / 2, 0);
    }

    /**
     * Sélectionne ce qui sera modifié par le slider
     * @param valuestr
     * @param el
     * @private
     */
    function _selectChangeValue(valuestr, el) {
        if (document.querySelector(".activev")) document.querySelector(".activev").classList.remove("activev");
        el.classList.add("activev");
        _current_variable = valuestr;
        _restoreValueInSlider();
    }

    /**
     * Récupération de la denière valeur à  mettre sur le slider
     * @private
     */
    function _restoreValueInSlider() {
        document.querySelector('input[type="range"]').value = _values[_current_variable];
    }

    /**
     * Change la valeur de la variable
     * @param v
     * @private
     */
    function _setNewValue(v) {
        _values[_current_variable] = 1 * v;
        _updateBrow();
    }

    /**
     * Attendre le chargement de l'asset
     * @param el
     * @param cb
     */
    function doWhenLoaded(el, cb) {
        if (el.getBounds().width > 1) {
            cb();
        } else {
            setTimeout(function () {
                doWhenLoaded(el, cb);
            }, 25);
        }
    }

    /**
     * Met à jour le dessin des sourcils sur le canvas
     * @private
     */
    function _updateBrow() {
        _brow.blendMode = PIXI.BLEND_MODES.OVERLAY;
        _brow.anchor.set(0.5);
        _brow.scale.y = _base_brow_scale + 1 * _values.thickness / 10 * 0.1;
        _brow.position.y = _base_y - 1 * _values.position;
        var r = 1 / 7;
        _brow.bl.position.x = _brow.br.position.x + 2 * _brow.br.width + (2 * r * _brow.br.width + 5 * _values.distance);
        _brow.position.x = _root.getBoundingClientRect().width / 2 - _brow.width / 2;

        var colorMatrix = new PIXI.filters.ColorMatrixFilter();
        _brow.filters = [colorMatrix];
        colorMatrix.hue(180 + 180 * _values.tint / 10, true);
        colorMatrix.browni(true);
    }

    /**
     * Créer les sourcils selon le sourcil sélectionné
     * @param br_num
     */
    function createBrow(brow_num, cb) {
        var b = new PIXI.Container();
        var br = new PIXI.Sprite.fromImage("img/brow-" + brow_num + "-r.png");
        b.addChild(br);
        br.anchor.set(0);
        var bl = new PIXI.Sprite.fromImage("img/brow-" + brow_num + "-r.png");
        b.addChild(bl);
        bl.anchor.set(0);
        b.bl = bl;
        b.br = br;
        setTimeout(function () {
            bl.position.y = br.position.y;
            bl.scale.x = -1;
            var r = 1 / 7;
            bl.position.x = br.position.x + 2 * br.width + 2 * r * br.width;
            cb(b);
        }, 250);
    }


    /**
     * Remove pixel of real brow
     * @param root
     * @private
     */
    function _removeRealBrow(root) {

    }

    /**
     * Dessine le fond
     * @param drawVariableImage
     * @param manageShot
     * @private
     */
    function _drawBackground(drawVariableImage, manageShot, after) {
        _app = new PIXI.Application({
            width: 240,
            height: 320,
            preserveDrawingBuffer: true,
            resolution: 1,
            clearBeforeRender: true,
            transparent: true
        });
        document.querySelector(".picturewrapper").appendChild(_app.view);

        var v = drawVariableImage(_app);

        var con = new PIXI.Container();
        _app.stage.addChild(con);

        var vline = new PIXI.Graphics();
        con.addChild(vline);
        vline.lineStyle(2, 0x000000, 0.7)
            .moveTo(120, 0)
            .lineTo(120, 320);


        var vovale = new PIXI.Graphics();
        con.addChild(vovale);
        vovale.beginFill(0xffffff, 0.1);
        vovale.drawEllipse(120, 160, 120, 160);
        vovale.endFill();

        manageShot(_app, con, v);

        var apparel = new PIXI.Sprite.fromImage('img/prendre-photo.png');
        con.addChild(apparel);

        if (!_app.autorun) {
            apparel.anchor.set(0.5);
            apparel.scale.set(0.4);
            apparel.position.set(200, 280);
            apparel.interactive = true;
            apparel.button = true;
            apparel.cursor = "pointer";

            apparel.pointerover = function (data) {
                apparel.scale.set(0.5);
            };
            apparel.pointerout = function (data) {
                apparel.scale.set(0.4);
            };
        }

        apparel.click = apparel.tap = function (data) {
            con.destroy();
            setTimeout(function () {
                var mignaturesprite = new PIXI.Graphics();
                _app.stage.addChild(mignaturesprite);
                mignaturesprite.addChild(v);

                mignaturesprite.position.set(0, 0);
                var image = _app.renderer.plugins.extract.image();
                image.style.position = "absolute";
                image.style.bottom = 0;
                image.style.left = "calc(50% - 120px)";
                image.style.width = "240px";
                image.style.zIndex = "9999";
                if (!_app.autorun) {
                    TweenMax.from(image, 0.25, {opacity: 0});
                } else {
                    TweenMax.set(image, {opacity: 0});
                }
                document.querySelector(".picturewrapper").appendChild(image);

                after(mignaturesprite, image, _app, con, v);
            }, _app.autorun ? 1000 : 25);
        };

        if (_app.autorun) {
            apparel.click();
        }
    }

    function _drawLeft(g, ap, p, mask) {
        if (p[15]) {
            console.log(p[15], p[16], p[17], p[18]);
            g.lineStyle(2, 0xff0000, 1);
            g.moveTo(p[15][0], p[15][1]);
            g.lineTo(p[16][0], p[16][1]);
            g.lineTo(p[17][0], p[17][1]);
            g.lineTo(p[18][0], p[18][1]);
        }
    }

    function _drawRight(g, ap, p, mask) {
        if (p[19]) {
            console.log(p[19], p[20], p[21], p[22]);
            g.lineStyle(2, 0xff0000, 1);
            g.moveTo(p[19][0], p[19][1]);
            g.lineTo(p[20][0], p[20][1]);
            g.lineTo(p[21][0], p[21][1]);
            g.lineTo(p[22][0], p[22][1]);
        }
    }

    function _startTracker(ap) {
        var gl = new PIXI.Graphics();
        ap.stage.addChild(gl);
        var gr = new PIXI.Graphics();
        ap.stage.addChild(gr);

        var ctracker = new clm.tracker();
        ctracker.init(pModel);
        var ticker = new PIXI.ticker.Ticker();
        ticker.stop();
        ticker.add(function () {
            var p = ctracker.getCurrentPosition();
            _drawLeft(gl, ap, p, null);
            _drawRight(gr, ap, p, null);

            if (ctracker.getConvergence() < 0.4) {
                ctracker.stop();
                ticker.stop();
                alert("mettre les sourcils");
            }
        });
        ctracker.start(ap.view);
        ticker.start();
    }


    return {
        /**
         * Initialisation de l'application et de la vue
         */
        init: function () {
            document.addEventListener("DOMContentLoaded", function (ev) {
                _config = _calculateStageSize(_config);
                _app = new PIXI.Application(_config);
                _root.appendChild(_app.view);
                _drawFace();
                _startTracker(_app);
            });
        },
        /**
         * Changement de sourcil
         * @param brow_num
         * @param el le bouton d'action
         */
        selectBaseBrow: function (brow_num, el) {
            var brow = PIXI.Sprite.fromImage('img/brow-' + brow_num + '-r.png');
            if (brow) {
                document.querySelector(".actives").classList.remove("actives");
                if (_brow) {
                    _app.stage.removeChild(_brow);
                }
                createBrow(brow_num, function (b) {
                    _brow = b;
                    _app.stage.addChild(_brow);
                    _initBrowPosition();
                    _updateBrow();
                });
                el.classList.add("actives");
            }
        },
        /**
         * Sélection d'un changement de valeurs
         * @param el
         */
        selectChangeValuePosition: function (el) {
            _selectChangeValue('position', el);
        },
        /**
         * Sélection d'un changement de valeurs
         * @param el
         */
        selectChangeValueDistance: function (el) {
            _selectChangeValue('distance', el);
        },
        /**
         * Sélection d'un changement de valeurs
         * @param el
         */
        selectChangeValueThickness: function (el) {
            _selectChangeValue('thickness', el);
        },
        /**
         * Sélection d'un changement de valeurs
         * @param el
         */
        selectChangeValueTint: function (el) {
            _selectChangeValue('tint', el);
        },
        /**
         * Change la valeur
         * @param el
         */
        changeValue: function (el) {
            _setNewValue(el.value);
        },

        /**
         * Initialisation du chargeur d'image
         */
        initUploader: function () {
            document.getElementById("pictureuploader").addEventListener("change", function () {
                EyeDesigner.displayImage(this);
            });
        },

        /**
         * Image prise en direct live
         */
        initSnapper: function () {
            var template = '<video id="player" style="display:none"></video>';

            [].forEach.call(document.querySelectorAll("button"), function (el) {
                el.style.display = "none";
            });

            document.querySelector(".wrapper").classList.add("running");
            document.querySelector(".picturewrapper").innerHTML = template;

            var player = document.getElementById('player');
            player.autoplay = true;
            var constraints = {video: true};

            navigator.mediaDevices.getUserMedia(constraints)
                .then(function (stream) {
                    player.srcObject = stream;
                });

            _drawBackground(function (_app) {
                var videotexture = PIXI.VideoBaseTexture.fromVideo(player);
                var videosprite = new PIXI.Sprite.from(videotexture);
                videosprite.scale.x *= -1;
                videosprite.anchor = new PIXI.Point(0.5, 0);
                videosprite.position = new PIXI.Point(120, 0);
                _app.stage.addChild(videosprite);
                return videosprite;
            }, function (_app, con, videosprite) {
            }, function (migniature, image, app, con, v) {
                if (window.confirm("L'image vous convient ?")) {
                    window.localStorage.setItem("image", image.src);
                    document.location = "etape2.html";
                } else {
                    document.location = "etape1.html";
                }
            });
        },

        /**
         * Image chargée
         * @param el
         */
        displayImage: function (el) {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        _drawBackground(function (_app) {
                            var pct = new PIXI.Sprite.fromImage(e.target.result);
                            _app.stage.addChild(pct);
                            doWhenLoaded(pct, function () {
                                pct.anchor.set(0.5);
                                pct.scale.set(Math.max(240 / pct.width, 360 / pct.height));
                                pct.position = new PIXI.Point(120, 180);
                            });
                            return pct;
                        }, function (_app, con, videosprite) {
                            _app.autorun = true;
                        }, function (migniature, image, app, con, v) {
                            window.localStorage.setItem("image", image.src);
                            document.location = "etape2.html";
                        });
                    };

                    reader.readAsDataURL(input.files[0]);
                    el.parentNode.removeChild(el);
                    document.querySelector(".wrapper").classList.add("moving");
                }
            }

            readURL(el);
        },

        reinitialiserSourcils: function () {
            document.location = "etape2.html";
        },

        avoirCesSourcils: function () {
            var image = _genererImagePourExport();
            var sourcils = recupererDonneesSourcil();
            if (image !== null && sourcils !== null) {
                var infos = demanderInfosClient();
                if (!!infos) {
                    var ret = envoyerEmail(infos.email, sourcils, image);
                    if (ret) {
                        afficherMessageEtPartirVers("Nous avons bien reçu les informations de vos sourcils !", 3000, "index.html");
                    } else {
                        afficherMessage("Une erreur nous a empêché de récupérer les informations de vos sourcils !");
                    }
                }
            }
        },

        partagerCesSourcils: function () {
            var image = _genererImagePourExport();
            if (image !== null) {
                posterSur(image, "facebook");
            }
        }
    };
})(PIXI);


